// Translates Luaparse AST output to Babel's expected AST input

const assert = require('assert');
const types = require('babel-types');


module.exports.translate = (ast) => {
  assert(typeof ast === 'object');
  assert(ast.type === 'Chunk');

  const statements = ast.body.map(translateNode);
  const program = types.program(statements);
  const file = types.file(program, [], []);

  return file;
};


function translateNode(node) {
  switch (node.type) {
    case 'CallStatement': return translateCallStatement(node);
    case 'StringCallExpression': return translateStringCallExpression(node);
    case 'Identifier': return translateIdentifier(node);
    case 'StringLiteral': return translateStringLiteral(node);

    default:
      console.log(node);
      throw new ReferenceError(`Unknown Luaparse AST node type: ${node.type}`);
  }
}


function translateCallStatement(node) {
  assert(node.expression);
  const expression = translateNode(node.expression);
  return types.expressionStatement(expression);
}


function translateStringCallExpression(node) {
  assert(node.base);
  assert(node.argument);

  const callee = translateNode(node.base);
  const args = [translateNode(node.argument)];

  return types.callExpression(callee, args);
}


function translateIdentifier(node) {
  assert(node.name);
  return types.identifier(node.name);
}


function translateStringLiteral(node) {
  assert(node.value);
  return types.stringLiteral(node.value);
}

