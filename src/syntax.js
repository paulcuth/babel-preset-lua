const parser = require('luaparse');
const translator = require('./ast-translator');

module.exports = () => ({
  manipulateOptions(opts) {
    opts.parserOpts = {
      parser(code) {
        const luaAst = parser.parse(code, { locations: true });
        const babelAst = translator.translate(luaAst);
        return babelAst;
      },
    };
  },
});
