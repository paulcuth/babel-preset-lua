const babel = require('babel-core');
const preset = require('../index');


function translate(source, expected) {
  test(source, () => {
    let result;

    try {
      result = babel.transform(source, {
        presets: [preset],
      }).code;

    } catch (e) {
      e.message += `\nin ${source}`;
      throw e;
    }

    expect(result).toBe(expected);
  });
}


translate('print "hello world"', 'print("hello world");');

