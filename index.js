
const syntax = require('./src/syntax');

module.exports = () => ({
  plugins: [
    syntax,
  ],
});

