# babel-preset-lua

This is a placeholder for work-in-progress to create a Babel preset for Lua, based on [Starlight](http://starlight.paulcuth.me.uk).

If you'd like to know more or help out, please get in touch by one of the following methods:
- [Starlight Gitter channel](https://gitter.im/paulcuth/starlight)
- [@lua_starlight on Twitter](https://twitter.com/lua_starlight)
